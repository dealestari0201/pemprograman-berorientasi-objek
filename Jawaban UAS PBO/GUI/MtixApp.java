import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MtixApp extends JFrame {
    private CardLayout cardLayout;
    private JPanel cardPanel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JButton signUpButton;
    private JTextField ticketNameField;
    private JTextField ticketPriceField;
    private JComboBox<String> paymentMethodComboBox;
    private JButton buyTicketButton;
    private JList<String> movieList;
    private DefaultListModel<String> movieListModel;

    public MtixApp() {
        super("mtix App");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set ukuran frame menjadi 80% dari ukuran layar
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) (screenSize.getWidth() * 0.8);
        int height = (int) (screenSize.getHeight() * 0.8);
        setPreferredSize(new Dimension(width, height));

        cardLayout = new CardLayout();
        cardPanel = new JPanel(cardLayout);

        // Login Panel
        JPanel loginPanel = createLoginPanel();
        cardPanel.add(loginPanel, "Login");

        // Sign Up Panel
        JPanel signUpPanel = createSignUpPanel();
        cardPanel.add(signUpPanel, "SignUp");

        // Movie List Panel
        JPanel movieListPanel = createMovieListPanel();
        cardPanel.add(movieListPanel, "MovieList");

        // Ticket Panel
        JPanel ticketPanel = createTicketPanel();
        cardPanel.add(ticketPanel, "BuyTicket");

        add(cardPanel);
        cardLayout.show(cardPanel, "Login");

        pack();
        setLocationRelativeTo(null);

        // Set warna background menjadi putih
        Color backgroundColor = new Color(255, 255, 255);
        getContentPane().setBackground(backgroundColor);

        setVisible(true);
    }

    private JPanel createLoginPanel() {
        JPanel loginPanel = new JPanel(new BorderLayout());
        JPanel inputPanel = new JPanel(new GridLayout(2, 2));
        JPanel buttonPanel = new JPanel(new FlowLayout());
        loginPanel.setBorder(BorderFactory.createTitledBorder("Login"));

        usernameField = new JTextField(10);
        passwordField = new JPasswordField(10);
        loginButton = new JButton("Login");
        signUpButton = new JButton("Sign Up");

        inputPanel.add(new JLabel("Username:"));
        inputPanel.add(usernameField);
        inputPanel.add(new JLabel("Password:"));
        inputPanel.add(passwordField);

        buttonPanel.add(loginButton);
        buttonPanel.add(signUpButton);

        loginPanel.add(inputPanel, BorderLayout.CENTER);
        loginPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Set warna tombol menjadi hijau botol
        Color buttonColor = new Color(34, 139, 34);
        loginButton.setBackground(buttonColor);
        signUpButton.setBackground(buttonColor);

        // Add action listener for login button
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });

        // Add action listener for sign up button
        signUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cardPanel, "SignUp");
            }
        });

        return loginPanel;
    }

   
    private JPanel createSignUpPanel() {
        JPanel signUpPanel = new JPanel(new BorderLayout());
        JPanel inputPanel = new JPanel(new GridLayout(2, 2));
        JPanel buttonPanel = new JPanel(new FlowLayout());
        signUpPanel.setBorder(BorderFactory.createTitledBorder("Sign Up"));

        usernameField = new JTextField(10);
        passwordField = new JPasswordField(10);
        JButton backButton = new JButton("Back");
        JButton signUpConfirmButton = new JButton("Sign Up");

        inputPanel.add(new JLabel("Username:"));
        inputPanel.add(usernameField);
        inputPanel.add(new JLabel("Password:"));
        inputPanel.add(passwordField);

        buttonPanel.add(backButton);
        buttonPanel.add(signUpConfirmButton);

        signUpPanel.add(inputPanel, BorderLayout.CENTER);
        signUpPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add action listener for back button
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cardPanel, "Login");
            }
        });

        // Add action listener for sign up confirm button
        signUpConfirmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signUp();
            }
        });

        return signUpPanel;
    }

    private JPanel createMovieListPanel() {
        JPanel movieListPanel = new JPanel(new BorderLayout());
        movieListPanel.setBorder(BorderFactory.createTitledBorder("Movie List"));
        movieListModel = new DefaultListModel<>();
        movieList = new JList<>(movieListModel);
        movieList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrollPane = new JScrollPane(movieList);
        movieListPanel.add(scrollPane);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        JButton continueButton = new JButton("Lanjutkan Beli Tiket");
        buttonPanel.add(continueButton);
        movieListPanel.add(buttonPanel, BorderLayout.SOUTH);

        populateMovieList();

        continueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = movieList.getSelectedIndex();
                if (selectedIndex != -1) {
                    cardLayout.show(cardPanel, "BuyTicket");
                    movieList.clearSelection();
                } else {
                    JOptionPane.showMessageDialog(MtixApp.this, "Pilih film terlebih dahulu.");
                }
            }
        });

        return movieListPanel;
    }

    private JPanel createTicketPanel() {
        JPanel ticketPanel = new JPanel(new BorderLayout());
        JPanel inputPanel = new JPanel(new GridLayout(3, 2)); // Update to 3 rows
        JPanel buttonPanel = new JPanel(new FlowLayout());
        ticketPanel.setBorder(BorderFactory.createTitledBorder("Buy Ticket"));

        ticketNameField = new JTextField(10);
        ticketPriceField = new JTextField(10);
        paymentMethodComboBox = new JComboBox<>(); // Combo box for selecting payment method
        buyTicketButton = new JButton("Buy Ticket");
        JButton backButton = new JButton("Back");

        inputPanel.add(new JLabel("Ticket Name:"));
        inputPanel.add(ticketNameField);
        inputPanel.add(new JLabel("Ticket Price:"));
        inputPanel.add(ticketPriceField);
        inputPanel.add(new JLabel("Payment Method:"));
        inputPanel.add(paymentMethodComboBox);

        buttonPanel.add(buyTicketButton);
        buttonPanel.add(backButton);

        ticketPanel.add(inputPanel, BorderLayout.CENTER);
        ticketPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add action listener for back button
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cardPanel, "MovieList");
            }
        });

        // Add action listener for buy ticket button
        buyTicketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buyTicket();
            }
        });

        // Populate payment method combo box
        paymentMethodComboBox.addItem("Credit");
        paymentMethodComboBox.addItem("OVO");
        paymentMethodComboBox.addItem("MTIX");

        return ticketPanel;
    }

    private void login() {
        String username = usernameField.getText();
        String password = new String(passwordField.getPassword());

        // Koneksi ke database
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/appmtix", "root", "");

            // Query untuk memeriksa keberadaan pengguna
            String query = "SELECT * FROM users WHERE username = ? AND password = ?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, username);
            statement.setString(2, password);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                JOptionPane.showMessageDialog(this, "Login successful");
                cardLayout.show(cardPanel, "MovieList");
            } else {
                JOptionPane.showMessageDialog(this, "Invalid username or password");
            }

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signUp() {
        String username = usernameField.getText();
        String password = new String(passwordField.getPassword());

        // Koneksi ke database
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/appmtix", "root", "");

            // Query untuk memeriksa keberadaan pengguna dengan username yang sama
            String query = "SELECT * FROM users WHERE username = ?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, username);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                JOptionPane.showMessageDialog(this, "Username already exists. Please choose a different username.");
            } else {
                // Query untuk menyimpan pengguna baru
                String insertQuery = "INSERT INTO users (username, password) VALUES (?, ?)";
                PreparedStatement insertStatement = conn.prepareStatement(insertQuery);
                insertStatement.setString(1, username);
                insertStatement.setString(2, password);
                insertStatement.executeUpdate();
                JOptionPane.showMessageDialog(this, "Sign up successful. You can now log in.");
                cardLayout.show(cardPanel, "Login");
            }

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateMovieList() {
        // Daftar film-film yang diketahui
        List<String> movies = new ArrayList<>();
        movies.add("Insidious The Red Door");
        movies.add("Hidden Strike");
        movies.add("Openheimer");
        movies.add("Kejar Mimpi Gaspol");
        movies.add("Elemental Forces Of Nature");

        for (String movie : movies) {
            movieListModel.addElement(movie);
        }
    }

    private void buyTicket() {
        String name = ticketNameField.getText();
        String price = ticketPriceField.getText();
        String paymentMethod = (String) paymentMethodComboBox.getSelectedItem(); // Get selected payment method

        try {
            // Membangun URL webservice dengan parameter yang diencode
            String urlStr = "http://localhost/app/index.php?action=belitiket";
            String params = "name=" + myURLEncode(name) +
                    "&price=" + myURLEncode(price) +
                    "&payment_method=" + myURLEncode(paymentMethod); // Add payment method parameter
            URL url = new URL(urlStr + "?" + params);

            // Membuat koneksi HTTP
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");

            // Menerima tanggapan dari webservice
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();

            // Menampilkan tanggapan dari webservice
            JOptionPane.showMessageDialog(this, response.toString());

            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String myURLEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new MtixApp();
            }
        });
    }
}