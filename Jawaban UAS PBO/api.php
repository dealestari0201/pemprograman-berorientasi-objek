<?php
header("Access-Control-Allow-Origin: *"); // Mengizinkan permintaan dari semua domain
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE"); // Mengizinkan metode HTTP yang diizinkan
header("Access-Control-Allow-Headers: Content-Type"); // Mengizinkan tipe konten yang diizinkan
require_once 'Database.php';
require_once 'User.php';
require_once 'Film.php';
require_once 'PembelianTiket.php';

$database = new Database();
$connection = $database->getConnection();
$user = new User($connection);
$film = new Film($connection);
$pembelianTiket = new PembelianTiket($connection);

// Implementasi endpoint untuk signup
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['action']) && $_GET['action'] === 'signup') {
    // Mendapatkan data dari permintaan
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Panggil metode createUser() dari kelas User
    $user->createUser($username, $password);

    // Berikan respons ke aplikasi mtix
    echo json_encode(['message' => 'Pengguna berhasil didaftarkan']);
    exit;
}

// Implementasi endpoint untuk login
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['action']) && $_GET['action'] === 'login') {
    // Mendapatkan data dari permintaan
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Panggil metode getUser() dari kelas User
    $userData = $user->getUser($username);

    // Verifikasi kata sandi pengguna
    if ($userData && password_verify($password, $userData['password'])) {
        // Berikan respons ke aplikasi mtix
        echo json_encode(['message' => 'Login berhasil']);
    } else {
        echo json_encode(['message' => 'Login gagal']);
    }
    exit;
}

// Implementasi endpoint untuk daftar film
if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['action']) && $_GET['action'] === 'film') {
    // Panggil metode getFilm() dari kelas Film
    $films = $film->getFilms();

    // Berikan respons ke aplikasi mtix
    echo json_encode(['films' => $films]);
    exit;
}

// Implementasi endpoint untuk pembelian tiket
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['action']) && $_GET['action'] === 'belitiket') {
    // Mendapatkan data dari permintaan
    $userId = $_POST['userId'];
    $filmId = $_POST['filmId'];
    $paymentMethod = $_POST['paymentMethod'];

    // Panggil metode beliTiket() dari kelas PembelianTiket
    $pembelianTiket->beliTiket($userId, $filmId, $paymentMethod);

    // Berikan respons ke aplikasi mtix
    echo json_encode(['message' => 'Pembelian tiket berhasil']);
    exit;
}

// Menampilkan pesan kesalahan jika tidak ada endpoint yang cocok
echo json_encode(['message' => 'Endpoint tidak valid']);

?>
