<?php
class User {
    private $connection;

    public function __construct($connection) {
        $this->connection = $connection;
    }

    public function createUser($username, $password) {
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $query = "INSERT INTO users (username, password) VALUES (?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ss", $username, $hashedPassword);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getUser($username) {
        $query = "SELECT * FROM users WHERE username = ?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("s", $username);
        $statement->execute();

        $result = $statement->get_result();
        $user = $result->fetch_assoc();

        return $user;
    }

    public function updateUser($username, $password) {
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $query = "UPDATE users SET password = ? WHERE username = ?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ss", $hashedPassword, $username);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteUser($username) {
        $query = "DELETE FROM users WHERE username = ?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("s", $username);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllUsers() {
        $query = "SELECT * FROM users";
        $result = $this->connection->query($query);

        $users = [];
        while ($row = $result->fetch_assoc()) {
            $users[] = $row;
        }

        return $users;
    }
}

?>
