<?php
class PembelianTiket {
    private $connection;

    public function __construct($connection) {
        $this->connection = $connection;
    }

    public function beliTiket($userId, $filmId, $paymentMethod) {
        $query = "INSERT INTO pembelian_tiket (user_id, film_id, payment_method) VALUES (?, ?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("iis", $userId, $filmId, $paymentMethod);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllPembelianTikets() {
        $query = "SELECT * FROM pembelian_tiket";
        $result = $this->connection->query($query);

        $pembelianTikets = [];
        while ($row = $result->fetch_assoc()) {
            $pembelianTikets[] = $row;
        }

        return $pembelianTikets;
    }
}

?>
