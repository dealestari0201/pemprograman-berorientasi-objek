<?php
class Film {
    private $connection;

    public function __construct($connection) {
        $this->connection = $connection;
    }

    public function createFilm($title, $description) {
        $query = "INSERT INTO films (title, description) VALUES (?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ss", $title, $description);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getFilm($filmId) {
        $query = "SELECT * FROM films WHERE id = ?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("i", $filmId);
        $statement->execute();

        $result = $statement->get_result();
        $film = $result->fetch_assoc();

        return $film;
    }

    public function updateFilm($filmId, $title, $description) {
        $query = "UPDATE films SET title = ?, description = ? WHERE id = ?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ssi", $title, $description, $filmId);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteFilm($filmId) {
        $query = "DELETE FROM films WHERE id = ?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("i", $filmId);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllFilms() {
        $query = "SELECT * FROM films";
        $result = $this->connection->query($query);

        $films = [];
        while ($row = $result->fetch_assoc()) {
            $films[] = $row;
        }

        return $films;
    }
}

?>
