<?php
require_once 'Database.php';
require_once 'User.php';
require_once 'Film.php';
require_once 'PembelianTiket.php';

$database = new Database();
$connection = $database->getConnection();
$user = new User($connection);
$film = new Film($connection);
$pembelianTiket = new PembelianTiket($connection);

// Mendapatkan data pengguna
$users = [];
$usersData = $user->getAllUsers();
if ($usersData) {
    foreach ($usersData as $userData) {
        $users[] = [
            'id' => $userData['id'],
            'username' => $userData['username']
        ];
    }
}

// Mendapatkan data film
$films = [];
$filmsData = $film->getAllFilms();
if ($filmsData) {
    foreach ($filmsData as $filmData) {
        $films[] = [
            'id' => $filmData['id'],
            'title' => $filmData['title'],
            'description' => $filmData['description']
        ];
    }
}

// Mendapatkan data pembelian tiket
$pembelianTikets = [];
$pembelianTiketsData = $pembelianTiket->getAllPembelianTikets();
if ($pembelianTiketsData) {
    foreach ($pembelianTiketsData as $pembelianTiketData) {
        $pembelianTikets[] = [
            'id' => $pembelianTiketData['id'],
            'user_id' => $pembelianTiketData['user_id'],
            'film_id' => $pembelianTiketData['film_id'],
            'payment_method' => $pembelianTiketData['payment_method'],
            'purchase_date' => $pembelianTiketData['purchase_date']
        ];
    }
}

// Tampilkan data dalam format JSON
$data = [
    'users' => $users,
    'films' => $films,
    'pembelian_tikets' => $pembelianTikets
];

header('Content-Type: application/json');
echo json_encode($data);
