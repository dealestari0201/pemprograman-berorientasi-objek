<?php
class Database {
    private $host = 'localhost';
    private $username = 'root'; // Menggunakan huruf kecil "root"
    private $password = ''; // Isikan kata sandi MySQL Anda jika ada
    private $database = 'appmtix';
    private $connection;

    public function __construct() {
        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database);
        if ($this->connection->connect_error) {
            die('Koneksi database gagal: ' . $this->connection->connect_error);
        }
    }

    public function getConnection() {
        return $this->connection;
    }
}

?>
