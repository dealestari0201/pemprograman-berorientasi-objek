# N0.1
[Use_case](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Use_Case_Keseluruhan.pdf)
[Use_Case_Table](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Use_Case_Keseluruhan.pdf)
# N0.2
[class_diagram](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/Untitled_Diagram-Page-4.drawio__5_.png)
# N0.3

SOLID adalah sekelompok prinsip desain perangkat lunak yang membantu dalam mengembangkan perangkat lunak yang mudah dipahami, dapat dipelihara, dan dapat diperluas. Berikut merupakan penerapan setiap poin dari SOLID Design Principles di dalam webservice:


1. **Single Responsibility Principle (SRP)**: Setiap kelas memiliki tanggung jawab yang terpisah dan hanya bertanggung jawab untuk satu hal. Misalnya, kelas `Database` bertanggung jawab untuk mengelola koneksi database, kelas `User` bertanggung jawab untuk mengelola entitas pengguna, dan kelas `Film` bertanggung jawab untuk mengelola entitas film. Setiap kelas ini memiliki tanggung jawab yang terpisah dan tidak saling campur aduk.

2. **Open-Closed Principle (OCP)**: Kode dirancang agar mudah diperluas tanpa mengubah kode yang sudah ada. Misalnya, jika ingin menambahkan fitur baru, seperti mengelola entitas pembelian tiket, Anda dapat membuat kelas `PembelianTiket` yang mengimplementasikan operasi-operasi terkait dan tidak perlu mengubah kode kelas-kelas yang sudah ada.

3. **Liskov Substitution Principle (LSP)**: Kode mengikuti prinsip substitusi Liskov, yang berarti objek dapat diganti dengan objek dari kelas turunan tanpa mengubah kebenaran dari program. Misalnya, kelas `User` dan `Film` dapat digunakan sebagai objek dari kelas turunan tanpa mempengaruhi kebenaran program.

4. **Interface Segregation Principle (ISP)**: Tidak ada implementasi.

5. **Dependency Inversion Principle (DIP)**: Dalam hal ini, prinsip inversi dependensi diterapkan melalui konstruktor. Misalnya, kelas `User` menerima objek `Database` melalui konstruktor, sehingga kelas `User` bergantung pada abstraksi `Database` daripada detail implementasinya. Hal ini memudahkan pengujian dan fleksibilitas dalam menggunakan sumber daya database.

Penerapan prinsip-prinsip SOLID ini membantu memastikan bahwa kode menjadi lebih terstruktur, mudah dipelihara, dan memudahkan perluasan di masa mendatang.
[Class_Database](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/Database.php)[Class_User](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/User.php)[Class_Film](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/Film.php)[Class_Pembelian_Tiket](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/PdmbelianTiket.php)


# N0.4

-Dalam webservice yang telah dibuat, terdapat penerapan beberapa pola desain (design pattern) yang umum digunakan dalam pengembangan perangkat lunak. Berikut adalah pola desain yang diterapkan dan penjelasannya:

1. **MVC (Model-View-Controller)**:
   - Pada webservice ini, pola desain MVC diterapkan dengan memisahkan logika bisnis (model), tampilan (view), dan pengendali (controller).
   - Model terwakili oleh kelas User dan Ticket yang berinteraksi dengan database dan melakukan operasi CRUD (Create, Read, Update, Delete) terhadap data.
   - View diimplementasikan menggunakan elemen-elemen antarmuka pengguna (UI) seperti JFrame, JPanel, JTextField, JButton, dan sebagainya.
   - Controller diimplementasikan melalui metode-metode seperti `handleGetRequest()`, `handlePostRequest()`, `handlePutRequest()`, dan `handleDeleteRequest()`, yang mengatur alur aplikasi dan menghubungkan logika bisnis dengan antarmuka pengguna.

2. **Factory Method**:
   - Pola desain Factory Method diterapkan melalui pembuatan objek User dan Ticket menggunakan metode create yang terpisah di kelas User dan Ticket.
   - Metode createUser() dalam kelas User dan metode createTicket() dalam kelas Ticket bertanggung jawab untuk menciptakan objek-objek baru sesuai dengan parameter yang diberikan.
   - Dengan menggunakan Factory Method, proses pembuatan objek terpisah dari kode utama dan memungkinkan fleksibilitas dalam membuat objek baru dengan logika tambahan jika diperlukan.

3. **Front Controller**:
   - Pola desain Front Controller digunakan dalam webservice ini melalui penggunaan fungsi `$_SERVER['REQUEST_METHOD']` untuk menentukan tindakan yang tepat berdasarkan metode permintaan HTTP.
   - Semua permintaan HTTP akan diteruskan melalui index.php, yang bertindak sebagai Front Controller, mengarahkan permintaan ke fungsi yang sesuai berdasarkan metode permintaan yang diterima.
   - Pola desain Front Controller memungkinkan konsolidasi pengelolaan permintaan dan mengurangi duplikasi kode dalam menangani permintaan yang berbeda.

4. **Dependency Injection**:
   - Pola desain Dependency Injection (DI) digunakan dalam webservice ini melalui injeksi objek User dan Ticket ke dalam metode-metode yang membutuhkannya.
   - Objek User dan Ticket diinjeksikan ke metode createUser(), createTicket(), updateUser(), updateTicket(), deleteUser(), dan deleteTicket().
   - Ini memisahkan logika bisnis webservice dari pembuatan objek User dan Ticket, memungkinkan penggantian objek dengan implementasi alternatif dan meningkatkan fleksibilitas serta pengujian unit.

Penerapan pola desain ini membantu memisahkan tanggung jawab, memperjelas struktur, meningkatkan fleksibilitas, dan memudahkan pemeliharaan webservice secara keseluruhan.
[Class_Database](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/Database.php)[Class_User](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/User.php)[Class_Film](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/Film.php)[Class_Pembelian_Tiket](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/PdmbelianTiket.php)

# NO.5

Dalam webservice dan GUI, konektivitas dengan database MySQL diimplementasikan menggunakan PHP. Berikut adalah penjelasan dan contoh implementasi konektivitas database dari webservice dan GUI:

1. Konektivitas Database dari Webservice:

- Pada file PHP index.php, diawali dengan pemanggilan require_once 'Database.php'; untuk memuat kelas Database yang berisi konfigurasi koneksi database.
- Setelah itu, objek Database dibuat dan koneksi database didapatkan melalui pemanggilan getConnection().
- Objek Database dan koneksi database tersebut kemudian digunakan untuk membuat objek User, Film, dan PembelianTiket yang diperlukan dalam implementasi webservice.
- Penggunaan koneksi database dalam kelas User, Film, dan PembelianTiket mengikuti pola Dependency Injection dengan menggunakan konstruktor.

  [Class_database](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/Jawaban%20UAS%20PBO/app/Database.php)

2. **Konektivitas Database di GUI**:

- Di dalam kelas MtixApp, terdapat metode login() dan signUp() yang melakukan koneksi ke database MySQL menggunakan JDBC.
- Pada kedua metode tersebut, koneksi ke database dibuat dengan menggunakan DriverManager.getConnection() dan diinisialisasi dengan URL, username, dan password yang sesuai.
- Setelah koneksi berhasil dibuat, query dieksekusi menggunakan objek PreparedStatement untuk melakukan operasi login atau sign up.
- Koneksi ditutup dengan memanggil conn.close() setelah query selesai dieksekusi.

[Mtixapp](Jawaban UAS PBO/GUI/MtixApp.java)

Dalam kedua kasus, konektivitas dengan database dilakukan menggunakan informasi koneksi seperti host, port, username, password, dan nama database yang sesuai. Setelah koneksi berhasil, query dieksekusi menggunakan metode-metode yang disediakan oleh API database yang digunakan (misalnya, mysqli_query() dalam PHP atau PreparedStatement dalam Java). Hasil query dapat diambil dan diolah sesuai kebutuhan. Setelah selesai, koneksi ke database ditutup untuk membebaskan sumber daya.


# NO.6
Terdapat implementasi sederhana dari sebuah webservice menggunakan PHP. Webservice ini menyediakan operasi CRUD (Create, Read, Update, Delete) untuk entitas pengguna (User) dan entitas film (Film). Berikut adalah penjelasan singkat tentang pembuatan webservice dan setiap operasi CRUD-nya:

1. Pembuatan Webservice:
   - File `index.php` berfungsi sebagai entry point atau titik masuk untuk webservice.
   - Pertama, file `Database.php` di-require untuk mendapatkan konfigurasi koneksi ke database.
   - Objek Database dibuat dan koneksi ke database didapatkan melalui pemanggilan `getConnection()`.
   - Setelah itu, objek `User`, `Film`, dan `PembelianTiket` dibuat menggunakan koneksi database yang telah didapatkan.

2. Operasi CRUD pada Entitas Pengguna (User):
   - Create: Operasi create dilakukan dengan metode HTTP POST dan menggunakan endpoint `?action=signup`.
   - Saat menerima permintaan create user, data username dan password diterima dari permintaan POST.
   - Metode `createUser()` pada objek User dipanggil untuk menyimpan pengguna baru ke database.
   - Jika penyimpanan berhasil, webservice memberikan respons dengan pesan "Pengguna berhasil didaftarkan".
   - Read: Operasi read dilakukan dengan metode HTTP POST dan menggunakan endpoint `?action=login`.
   - Saat menerima permintaan login, data username dan password diterima dari permintaan POST.
   - Metode `getUser()` pada objek User dipanggil untuk mendapatkan data pengguna dari database berdasarkan username yang diberikan.
   - Jika pengguna ditemukan dan kata sandi cocok, webservice memberikan respons dengan pesan "Login berhasil".
   - Update dan Delete: Operasi update dan delete pada entitas pengguna tidak diimplementasikan dalam contoh kode tersebut.

3. Operasi CRUD pada Entitas Film (Film):
   - Read: Operasi read dilakukan dengan metode HTTP GET dan menggunakan endpoint `?action=film`.
   - Saat menerima permintaan get films, metode `getFilms()` pada objek Film dipanggil untuk mendapatkan data semua film dari database.
   - Data film yang diperoleh dikirim sebagai respons webservice.
   - Create, Update, dan Delete: Operasi create, update, dan delete pada entitas film tidak diimplementasikan dalam contoh kode tersebut.

Implementasi webservice sederhana ini memanfaatkan mekanisme dasar dalam PHP untuk menangani permintaan HTTP dan mengakses database menggunakan kelas-kelas yang telah dibuat. 

[app_keseluruhan_file_program](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/tree/main/Jawaban%20UAS%20PBO/app)

# NO.7

Dalam aplikasi MTix, antarmuka pengguna grafis (Graphical User Interface/GUI) digunakan untuk memberikan interaksi yang mudah dan intuitif kepada pengguna. Berikut adalah penjelasan tentang antarmuka pengguna grafis dari aplikasi MTix:

1. Login Panel:
   - Panel ini digunakan untuk memasukkan informasi login pengguna.
   - Terdapat dua input field, yaitu "Username" dan "Password" yang digunakan untuk memasukkan informasi login.
   - Terdapat dua tombol, yaitu "Login" untuk melakukan login dan "Sign Up" untuk pindah ke panel pendaftaran.

2. Sign Up Panel:
   - Panel ini digunakan untuk pendaftaran pengguna baru.
   - Terdapat dua input field, yaitu "Username" dan "Password" yang digunakan untuk memasukkan informasi pendaftaran.
   - Terdapat dua tombol, yaitu "Back" untuk kembali ke panel login dan "Sign Up" untuk melakukan pendaftaran.

3. Movie List Panel:
   - Panel ini menampilkan daftar film yang tersedia.
   - Daftar film ditampilkan dalam bentuk daftar dengan menggunakan komponen JList.
   - Di bawah daftar film, terdapat tombol "Lanjutkan Beli Tiket" yang akan mengarahkan pengguna ke panel pembelian tiket saat film dipilih.

4. Ticket Panel:
   - Panel ini digunakan untuk melakukan pembelian tiket.
   - Terdapat tiga input field, yaitu "Ticket Name" untuk memasukkan nama tiket, "Ticket Price" untuk memasukkan harga tiket, dan "Payment Method" untuk memilih metode pembayaran.
   - Terdapat dua tombol, yaitu "Buy Ticket" untuk melakukan pembelian tiket dan "Back" untuk kembali ke panel daftar film.

Selain panel-panel di atas, aplikasi MTix juga menggunakan beberapa dialog box untuk menampilkan pesan atau konfirmasi kepada pengguna, seperti dialog untuk menampilkan pesan kesalahan saat login gagal atau pesan sukses saat pembelian tiket berhasil.

Antarmuka pengguna grafis pada aplikasi MTix didesain secara intuitif dengan penggunaan elemen-elemen GUI seperti input fields, tombol, dan daftar film. Hal ini memudahkan pengguna untuk melakukan operasi login, pendaftaran, memilih film, dan pembelian tiket dengan mudah.

[mtixapp](Jawaban UAS PBO/GUI/MtixApp.java)

# N0.8
Dalam aplikasi MTix, terdapat koneksi HTTP melalui GUI untuk berkomunikasi dengan webservice yang menyediakan operasi CRUD. Berikut adalah penjelasan tentang HTTP connection melalui GUI dalam aplikasi MTix:

1. Pembelian Tiket (Buy Ticket):
   - Ketika pengguna melakukan pembelian tiket melalui antarmuka pengguna GUI, ada HTTP connection yang dibuat.
   - Pertama, data tiket yang dimasukkan oleh pengguna, seperti nama tiket, harga tiket, dan metode pembayaran, dikumpulkan.
   - Setelah itu, URL endpoint webservice untuk pembelian tiket diinisialisasi dengan parameter yang diencode sesuai dengan data tiket.
   - Objek HttpURLConnection dibuat dan diatur metode HTTP-nya sebagai POST.
   - Koneksi dibuat dengan menggunakan URL dan metode POST, kemudian permintaan dikirimkan ke webservice.
   - Webservice menerima permintaan, memproses data tiket, dan mengembalikan respons.
   - Aplikasi MTix menerima tanggapan dari webservice melalui HttpURLConnection.
   - Tanggapan ditampilkan kepada pengguna melalui dialog box yang menampilkan pesan sukses atau pesan error tergantung pada respons yang diterima.

2. Login:
   - Saat pengguna melakukan login melalui antarmuka pengguna GUI, HTTP connection juga digunakan.
   - Data login pengguna, yaitu username dan password, dikumpulkan dari input fields pada GUI.
   - URL endpoint webservice untuk login diinisialisasi dengan parameter yang sesuai dengan data login.
   - Objek HttpURLConnection dibuat dan diatur metode HTTP-nya sebagai POST.
   - Koneksi dibuat dengan menggunakan URL dan metode POST, kemudian permintaan dikirimkan ke webservice.
   - Webservice menerima permintaan, memverifikasi data login, dan mengembalikan respons.
   - Aplikasi MTix menerima tanggapan dari webservice melalui HttpURLConnection.
   - Tanggapan ditampilkan kepada pengguna melalui dialog box yang menampilkan pesan sukses atau pesan error tergantung pada respons yang diterima.

Melalui koneksi HTTP ini, aplikasi MTix dapat berkomunikasi dengan webservice untuk melakukan operasi pembelian tiket dan login. Koneksi HTTP ini memungkinkan aplikasi untuk mengirimkan permintaan dan menerima respons dari webservice, sehingga memungkinkan interaksi antara aplikasi dan webservice yang terjadi di balik layar.
  
[mtixapp](Jawaban UAS PBO/GUI/MtixApp.java)
# NO.9
[youtube](https://youtu.be/OScOro1ys4U)
[ss](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/tree/main/Jawaban%20UAS%20PBO/SS)
# NO.10
