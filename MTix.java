import java.util.Scanner;

public class MTix {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        String movieName, cinemaName;
        int ticketPrice;

        System.out.println("--------------------------------");
        System.out.println("Cinema XXI");
        System.out.println("--------------------------------");

        boolean isLoggedIn = false;
        String email = "";
        String password = "";

        while (!isLoggedIn) {
            System.out.println("--------------------------------");
            System.out.println("Menu:");
            System.out.println("--------------------------------");
            System.out.println("1. Sign Up");
            System.out.println("2. Login");
            System.out.println("3. Logout");
            System.out.println("--------------------------------");
            System.out.print("Pilih menu (1/2/3): ");
            int menuChoice = input.nextInt();

            switch (menuChoice) {
                case 1:
                    System.out.print("Email: ");
                    email = input2.nextLine();
                    System.out.print("Password: ");
                    password = input2.nextLine();

                    SignUp signUp = new SignUp(email, password);
                    if (signUp.register()) {
                        System.out.println("Pendaftaran berhasil! Silakan masuk.");
                    } else {
                        System.out.println("Username sudah ada. Pendaftaran gagal.");
                    }
                    break;
                case 2:
                    System.out.print("Email: ");
                    email = input2.nextLine();
                    System.out.print("Password: ");
                    password = input2.nextLine();

                    Login login = new Login(email, password);

                    if (login.authenticate()) {
                        System.out.println("Login berhasil!");
                        isLoggedIn = true;
                    } else {
                        System.out.println("Username atau password tidak valid. Login gagal.");
                    }
                    break;
                case 3:
                    System.out.println("Keluar dari program...");
                    return;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih menu yang tersedia.");
                    break;
            }
            System.out.println();
        }

        boolean exit = false;
        while (!exit) {
            System.out.println("--------------------------------");
            System.out.println("Menu:");
            System.out.println("--------------------------------");
            System.out.println("1. Daftar Film");
            System.out.println("2. Daftar Bioskop");
            System.out.println("3. Daftar Studio");
            System.out.println("4. Beli Tiket");
            System.out.println("5. Pembayaran");
            System.out.println("6. Keluar");
            System.out.println("--------------------------------");
            System.out.print("Pilih menu (1/2/3/4/5): ");
            int choice = input.nextInt();

            switch (choice) {
            case 1:
                System.out.println("\nDaftar Film:");
                System.out.println("--------------------------------");
                System.out.println("1. EVIL DEAD RISE, Horror, Rp. 25000");
                System.out.println("   Jadwal: 10:00, 13:00, 16:00");
                System.out.println("2. FAST X, Action - Rp. 40.000");
                System.out.println("   Jadwal: 11:00, 14:00, 17:00");
                System.out.println("3. Buya Hamka, Drama - Rp. 30.000");
                System.out.println("   Jadwal: 12:00, 15:00, 18:00");
                break;
                case 2:
                    System.out.println("\nDaftar Bioskop:");
                    System.out.println("--------------------------------");
                    System.out.println("1. Cinema XXI");
                    System.out.println("2. Studio 21");
                    System.out.println("3. Cinemaxx");
                    break;
                case 3:
                	   System.out.println("\nDaftar Studio:");
                	   System.out.println("--------------------------------");
                       System.out.println("1. Studio 1");
                       System.out.println("2. Studio 2");
                       System.out.println("3. Studio 3");
                       break;
                case 4:
                    // get movie info
                    System.out.print("Masukkan nama film: ");
                    movieName = input2.nextLine();

                    // get cinema info
                    System.out.print("Masukkan nama bioskop: ");
                    cinemaName = input2.nextLine();
                    
                    
                    System.out.print("Masukkan harga tiket: ");
                    ticketPrice = input.nextInt();

                    // create movie object
                    Movie movie = new Movie(movieName);

                    // create cinema object
                    Cinema cinema = new Cinema(cinemaName, ticketPrice);
                    
                 // get studio info
                    System.out.println("--------------------------------");
                    System.out.println("Daftar Studio:");
                    System.out.println("--------------------------------");
                    System.out.println("1. Studio 1");
                    System.out.println("2. Studio 2");
                    System.out.println("3. Studio 3");
                    System.out.println("--------------------------------");
                    System.out.print("Pilih studio (1/2/3): ");
                    int studioChoice = input.nextInt();
                    String studioName;
                    
                    switch (studioChoice) {
                        case 1:
                            studioName = "Studio 1";
                            break;
                        case 2:
                            studioName = "Studio 2";
                            break;
                        case 3:
                            studioName = "Studio 3";
                            break;
                        default:
                            System.out.println("Pilihan tidak valid. Studio default digunakan.");
                            studioName = "Studio Default";
                            break;
                    }

                    // create studio object
                    Studio studio = new Studio(studioName);

                    // display selected studio
                    System.out.println("Studio: " + studio.getName());
                    
                    // display movie and cinema info
                    System.out.println("-----------------------------------------------------");
                    System.out.println("\nFilm: " + movie.getName()+ "Bioskop: " + cinema.getName() + " (harga tiket: " + cinema.getTicketPrice() + ")");
                    
                    System.out.println("--------------------------------");
                    System.out.println("Jenis Tiket:");
                    System.out.println("--------------------------------");
                    System.out.println("1. Regular");
                    System.out.println("2. Premium");
                    System.out.println("--------------------------------");
                    System.out.print("Pilih jenis tiket (1/2): ");
                    int ticketType = input.nextInt();

                    // get ticket quantity
                    System.out.print("Masukkan jumlah tiket yang dibeli: ");
                    int ticketQuantity = input.nextInt();
                    
                 // create ticket object
                    Ticket ticket;
                    if (ticketType == 1) {
                        ticket = new Ticket(movie, cinema, ticketQuantity);
                    } else if (ticketType == 2) {
                        System.out.print("Masukkan harga snack: ");
                        int snackPrice = input.nextInt();
                        ticket = new PremiumTicket(movie, cinema, ticketQuantity, snackPrice);
                    } else {
                        System.out.println("Pilihan tidak valid. Tiket tidak dibeli.");
                        break;
                    }

                    // create ticket object
                    System.out.println("--------------------------------");
                    System.out.println("Pilih nomor kursi:");
                    System.out.println("--------------------------------");
                    System.out.println("1. A1");
                    System.out.println("2. A2");
                    System.out.println("3. A3");
                    // ... (add more seat numbers as needed)
                    System.out.println("--------------------------------");
                    System.out.print("Pilih nomor kursi (1/2/3/...): ");
                    int seatChoice = input.nextInt();

                    // create seat number object
                    SeatNumber seatNumber = new SeatNumber(seatChoice);

                    // display ticket info, seat number, and total price
                    System.out.println("Anda telah membeli " + ticket.getTicketQuantity() + " tiket untuk film " + ticket.getMovie().getName() + " di bioskop " + ticket.getCinema().getName() + ".");
                    System.out.println("Nomor kursi: " + seatNumber.getNumber());
                    System.out.println("Total harga: " + ticket.getTotalPrice());
                case 5:
                    // Payment menu
                    System.out.println("--------------------------------");
                    System.out.println("Metode Pembayaran:");
                    System.out.println("--------------------------------");
                    System.out.println("1. OVO");
                    System.out.println("2. M.Tix Point");
                    System.out.println("--------------------------------");
                    System.out.print("Pilih metode pembayaran (1/2): ");
                    int paymentMethodChoice = input.nextInt();
                    PaymentMethod paymentMethod;
                    if (paymentMethodChoice == 1) {
                        paymentMethod = new PaymentMethod("OVO");
                    } else if (paymentMethodChoice == 2) {
                        paymentMethod = new PaymentMethod("M.Tix Point");
                    } else {
                        System.out.println("Pilihan tidak valid. Pembayaran dibatalkan.");
                        break;
                    }
                    // Process payment
                    System.out.println("Pembayaran dengan metode " + paymentMethod.getMethodName() + " berhasil.");
                    System.out.println("Terima kasih telah menggunakan layanan kami.");
                    break;
                case 6:
                    exit = true;
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih menu yang tersedia.");
                    break;
            }
        }
        input.close();
        input2.close();
    }
}

    class SignUp {
    private String email;
    private String password;
    public SignUp(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public boolean register() {
        return true;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
   }

    class Login {
    private String email;
    private String password;
    public Login(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public boolean authenticate() {
        return true;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
        public void setPassword(String password) {
            this.password = password;
        }
    }

    class Movie {
    private String name;
    public Movie(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

    class Genre extends Movie {
    private String genre;
    public Genre(String name, String genre) {
        super(name);
        this.genre = genre;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
    
    class Date {
        private int day;
        private int month;
        private int year;

        public Date(int day, int month, int year) {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }
    }

    class Time {
        private int hours;
        private int minutes;

        public Time(int hours, int minutes) {
            this.hours = hours;
            this.minutes = minutes;
        }

        public int getHours() {
            return hours;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }

        public int getMinutes() {
            return minutes;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }
    }
    
    class Cinema {
    private String name;
    private int ticketPrice;
    public Cinema(String name, int ticketPrice) {
        this.name = name;
        this.ticketPrice = ticketPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }
}

    class CinemaList {
        private String[] cinemas;
        public CinemaList() {
            this.cinemas = new String[]{"Cinema XXI", "CGV", "Cineplex", "Empire XXI", "Mega Cineplex"};
        }

        public String[] getCinemas() {
            return cinemas;
        }

        public void setCinemas(String[] cinemas) {
            this.cinemas = cinemas;
        }
}
    

class Studio {
    private String name;

    public Studio(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class SeatNumber {
    private String number;

    public SeatNumber(int choice) {
        switch (choice) {
            case 1:
                this.number = "A1";
                break;
            case 2:
                this.number = "A2";
                break;
            case 3:
                this.number = "A3";
                break;
            // ... (add more seat numbers as needed)
            default:
                this.number = "Unknown";
                break;
        }
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
    
    class Ticket {
    private Movie movie;
    private Cinema cinema;
    private int ticketQuantity;
    
    public Ticket(Movie movie, Cinema cinema, int ticketQuantity) {
        this.movie = movie;
        this.cinema = cinema;
        this.ticketQuantity = ticketQuantity;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public int getTicketQuantity() {
        return ticketQuantity;
    }

    public void setTicketQuantity(int ticketQuantity) {
        this.ticketQuantity = ticketQuantity;
    }

    public int getTotalPrice() {
        return cinema.getTicketPrice() * ticketQuantity;
    }
}

    class PremiumTicket extends Ticket {
    private int snackPrice;
    public PremiumTicket(Movie movie, Cinema cinema, int ticketQuantity, int snackPrice) {
        super(movie, cinema, ticketQuantity);
        this.snackPrice = snackPrice;
    }

    public int getSnackPrice() {
        return snackPrice;
    }

    public void setSnackPrice(int snackPrice) {
        this.snackPrice = snackPrice;
    }

    
    public int getTotalPrice() {
        return super.getTotalPrice() + snackPrice;
    }
 }

    class PaymentMethod {
    private String methodName;
    public PaymentMethod(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
 }




  
