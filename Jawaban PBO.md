# No.1

Program yang saya buat menggunakan algoritma yang menggabungkan konsep perulangan dan pemilihan (looping and branching). Algoritma tersebut dirancang untuk memungkinkan pengguna mendaftar, login, dan melakukan beberapa tindakan terkait pembelian tiket bioskop, termasuk memilih film, bioskop, studio, jenis tiket, nomor kursi, dan metode pembayaran. Program menggunakan pernyataan switch-case untuk memilih tindakan berdasarkan input pengguna dan perulangan while untuk menjaga pengguna tetap berada dalam menu hingga mereka memilih untuk keluar.

Berikut merupakan contoh Algoritma dalam program berupa pseudecode:

Procedure Main():
    Initialize Scanner input and input2

    Declare variables:
        movieName, cinemaName: String
        ticketPrice: Integer
        isLoggedIn: Boolean
        email, password: String

    Display Cinema XXI header

    isLoggedIn = False
    email = ""
    password = ""

    While isLoggedIn is False:
        Display Menu options
        Read menuChoice from user

        Switch menuChoice:
            Case 1:
                Read email and password from user
                Create SignUp object with email and password
                If signUp.register() returns True:
                    Display successful registration message
                Else:
                    Display registration failure message
                End switch

            Case 2:
                Read email and password from user
                Create Login object with email and password
                If login.authenticate() returns True:
                    Set isLoggedIn to True
                    Display successful login message
                Else:
                    Display invalid username or password message
                End switch

            Case 3:
                Display exit message
                Exit program

            Default:
                Display invalid menu choice message
        End while

    Set exit to False

    While exit is False:
        Display Menu options
        Read choice from user

        Switch choice:
            Case 1:
                Display movie list
            Case 2:
                Display cinema list
            Case 3:
                Display studio list
            Case 4:
                Read movieName, cinemaName, and ticketPrice from user
                Create Movie object with movieName
                Create Cinema object with cinemaName and ticketPrice
                Display studio list
                Read studioChoice from user
                Create Studio object based on studioChoice
                Display selected studio
                Display movie and cinema info
                Display ticket type options
                Read ticketType from user
                Read ticketQuantity from user
                If ticketType is 1:
                    Create Ticket object with movie, cinema, and ticketQuantity
                Else if ticketType is 2:
                    Read snackPrice from user
                    Create PremiumTicket object with movie, cinema, ticketQuantity, and snackPrice
                Else:
                    Display invalid ticket type message
                    Break
                Display seat number options
                Read seatChoice from user
                Create SeatNumber object with seatChoice
                Display ticket, seat number, and total price information
            Case 5:
                Display payment method options
                Read paymentMethodChoice from user
                If paymentMethodChoice is 1:
                    Create PaymentMethod object with "OVO"
                Else if paymentMethodChoice is 2:
                    Create PaymentMethod object with "M.Tix Point"
                Else:
                    Display invalid payment method message
                    Break
                Display successful payment message
                Display thank you message
            Case 6:
                Set exit to True
            Default:
                Display invalid menu choice message
        End while

    Close Scanner input and input2
End Procedure


Berikut source code yang dibuat algoritma pada class MTix:

[MTix](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/MTix.java)

# N0.2

Penjelasan Algoritma yang dibuat:

Berikut adalah penjelasan lebih rinci tentang algoritma dalam program:

1. Inisialisasi Scanner `input` dan `input2`: Langkah pertama adalah membuat objek Scanner `input` dan `input2` untuk membaca input dari pengguna.

2. Deklarasikan variabel: Mendeklarasikan variabel-variabel yang akan digunakan dalam program. Variabel tersebut adalah `movieName` untuk menyimpan nama film, `cinemaName` untuk menyimpan nama bioskop, `ticketPrice` untuk menyimpan harga tiket, `isLoggedIn` untuk menandai status login pengguna, dan `email` serta `password` untuk menyimpan informasi login.

3. Tampilkan header Cinema XXI: Menampilkan judul atau header program, dalam hal ini "Cinema XXI", untuk memberi tahu pengguna tentang program yang sedang dijalankan.

4. Loop while untuk login: Program akan berada dalam loop while selama pengguna belum login. Hal ini dicapai dengan menggunakan variabel `isLoggedIn` yang awalnya diatur sebagai `False`.

5. Tampilkan menu pilihan: Menampilkan pilihan menu yang tersedia kepada pengguna, seperti "Sign Up", "Login", dan "Logout".

6. Switch-case untuk memproses menu: Menggunakan switch-case untuk memproses pilihan menu yang dipilih oleh pengguna. Setiap pilihan menu akan memiliki tindakan yang sesuai.

    a. Jika pengguna memilih "Sign Up", program akan meminta pengguna memasukkan email dan password untuk melakukan pendaftaran. Kemudian, program akan membuat objek SignUp dengan email dan password yang dimasukkan pengguna, dan memanggil metode `register()` untuk melakukan pendaftaran. Jika pendaftaran berhasil, pesan sukses ditampilkan; jika tidak, pesan kegagalan ditampilkan.

    b. Jika pengguna memilih "Login", program akan meminta pengguna memasukkan email dan password untuk melakukan login. Kemudian, program akan membuat objek Login dengan email dan password yang dimasukkan pengguna, dan memanggil metode `authenticate()` untuk memverifikasi login. Jika login berhasil, variabel `isLoggedIn` diatur sebagai `True`, dan pesan sukses ditampilkan; jika tidak, pesan kegagalan ditampilkan.

    c. Jika pengguna memilih "Logout", program akan menampilkan pesan keluar dan program akan selesai.

7. Tutup loop while login.

8. Set variabel `exit` sebagai `False`: Variabel `exit` digunakan untuk memastikan program berjalan dalam loop while berikutnya.

9. Loop while untuk menu utama: Program akan berada dalam loop while untuk menu utama setelah pengguna berhasil login.

10. Tampilkan menu pilihan: Menampilkan pilihan menu utama kepada pengguna, seperti "Daftar Film", "Daftar Bioskop", dan lain-lain.

11. Switch-case untuk memproses menu utama: Menggunakan switch-case untuk memproses pilihan menu utama yang dipilih oleh pengguna. Setiap pilihan menu akan memiliki tindakan yang sesuai.

    a. Jika pengguna memilih "Daftar Film", program akan menampilkan daftar film yang tersedia.

    b. Jika pengguna memilih "Daftar Bioskop", program akan menampilkan daftar bioskop yang tersedia.

    c. Jika pengguna memilih "Daftar Studio", program akan menampilkan daftar studio yang tersedia.

    d. Jika pengengguna memilih "Beli Tiket", program akan meminta pengguna memasukkan informasi film, bioskop, dan harga tiket. Kemudian, program akan membuat objek Movie dengan nama film yang dimasukkan pengguna, objek Cinema dengan nama bioskop dan harga tiket yang dimasukkan pengguna, dan objek Studio dengan nama studio yang dipilih pengguna. Setelah itu, program akan menampilkan informasi film, bioskop, dan studio yang dipilih.
    e. Jika pengguna memilih "Pembayaran", program akan menampilkan pilihan metode pembayaran kepada pengguna, seperti "OVO" dan "M.Tix Point". Setelah pengguna memilih metode pembayaran, program akan membuat objek PaymentMethod dengan nama metode pembayaran yang dipilih pengguna. Kemudian, program akan menampilkan pesan sukses pembayaran dengan metode yang dipilih.
    f. Jika pengguna memilih "Keluar", program akan mengubah nilai variabel `exit` menjadi `True`, sehingga program keluar dari loop utama.

12. Tutup loop while menu utama.

Tutup objek Scanner: Setelah program selesai, objek Scanner input dan input2 ditutup.
Algoritma ini mengikuti alur yang terstruktur dengan penggunaan loop while dan switch-case untuk memproses pilihan menu yang dipilih oleh pengguna. Program memanfaatkan objek-objek seperti Movie, Cinema, Studio, Ticket, dan PaymentMethod untuk menyimpan informasi dan melakukan operasi yang diperlukan.

Berikut source code yang dibuat algoritma pada class MTix:

[MTix](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/MTix.java)


# No.3
Konsep dasar OOP (Object-Oriented Programming) adalah pendekatan dalam pemrograman yang menggunakan objek sebagai unit utama untuk memodelkan dan memecahkan masalah. Program yang saya buat merupakan contoh implementasi OOP dalam bahasa pemrograman Java. Berikut adalah penjelasan singkat mengenai class, atribut, metode, objek, dan properti yang ada dalam program MTix:

Pada program di saya buat, terdapat beberapa class, metode, atribut, objek, dan properti yang dapat dijelaskan sebagai berikut:

1. Class:
   - MTix: Kelas utama yang berisi metode `main` sebagai titik masuk program.

2. Metode:
   - `main(String[] args)`: Metode utama yang dieksekusi saat program dimulai. Berisi alur logika program dan interaksi dengan pengguna.
   - `register()`: Metode dalam kelas `SignUp` yang digunakan untuk melakukan pendaftaran pengguna baru.
   - `authenticate()`: Metode dalam kelas `Login` yang digunakan untuk melakukan otentikasi pengguna.
   - `getName()`, `setName()`: Metode dalam kelas `Movie` yang digunakan untuk mengambil dan mengatur nama film.
   - `getGenre()`, `setGenre()`: Metode dalam kelas `Genre` yang digunakan untuk mengambil dan mengatur genre film.
   - `getDay()`, `setDay()`, `getMonth()`, `setMonth()`, `getYear()`, `setYear()`: Metode dalam kelas `Date` yang digunakan untuk mengambil dan mengatur tanggal.
   - `getHours()`, `setHours()`, `getMinutes()`, `setMinutes()`: Metode dalam kelas `Time` yang digunakan untuk mengambil dan mengatur waktu.
   - `getName()`, `setName()`: Metode dalam kelas `Cinema` yang digunakan untuk mengambil dan mengatur nama bioskop.
   - `getTicketPrice()`, `setTicketPrice()`: Metode dalam kelas `Cinema` yang digunakan untuk mengambil dan mengatur harga tiket bioskop.
   - `getCinemas()`, `setCinemas()`: Metode dalam kelas `CinemaList` yang digunakan untuk mengambil dan mengatur daftar bioskop.
   - `getName()`, `setName()`: Metode dalam kelas `Studio` yang digunakan untuk mengambil dan mengatur nama studio.
   - `getNumber()`, `setNumber()`: Metode dalam kelas `SeatNumber` yang digunakan untuk mengambil dan mengatur nomor kursi.
   - `getMovie()`, `setMovie()`: Metode dalam kelas `Ticket` yang digunakan untuk mengambil dan mengatur objek film.
   - `getCinema()`, `setCinema()`: Metode dalam kelas `Ticket` yang digunakan untuk mengambil dan mengatur objek bioskop.
   - `getTicketQuantity()`, `setTicketQuantity()`: Metode dalam kelas `Ticket` yang digunakan untuk mengambil dan mengatur jumlah tiket.
   - `getTotalPrice()`: Metode dalam kelas `Ticket` dan `PremiumTicket` yang digunakan untuk menghitung harga total tiket.
   - `getSnackPrice()`, `setSnackPrice()`: Metode dalam kelas `PremiumTicket` yang digunakan untuk mengambil dan mengatur harga snack.
   - `getMethodName()`, `setMethodName()`: Metode dalam kelas `PaymentMethod` yang digunakan untuk mengambil dan mengatur nama metode pembayaran.

3. Atribut:
   - email, password: Atribut dalam kelas `SignUp` dan `Login` yang digunakan untuk menyimpan email dan password pengguna.
   - name: Atribut dalam kelas `Movie`, `Genre`, `Cinema`, dan `Studio` yang digunakan untuk menyimpan nama film, genre film.
   - rating: Atribut dalam kelas Movie yang digunakan untuk menyimpan rating atau peringkat film.
   - releaseDate: Atribut dalam kelas Movie yang digunakan untuk menyimpan tanggal rilis film.
   - description: Atribut dalam kelas Movie yang digunakan untuk menyimpan deskripsi film
   - duration: Atribut dalam kelas Movie yang digunakan untuk menyimpan durasi film.
   - moviesList: Atribut dalam kelas MovieList yang digunakan untuk menyimpan daftar film.
   - genresList: Atribut dalam kelas GenreList yang digunakan untuk menyimpan daftar genre film.
   - cinemasList: Atribut dalam kelas CinemaList yang digunakan untuk menyimpan daftar bioskop.
   - studiosList: Atribut dalam kelas StudioList yang digunakan untuk menyimpan daftar studio dalam sebuah bioskop.
   - capacity: Atribut dalam kelas Cinema dan Studio yang digunakan untuk menyimpan kapasitas tempat duduk.
   - availableSeats: Atribut dalam kelas Cinema dan Studio yang digunakan untuk menyimpan jumlah kursi yang tersedia.
   - seatNumber: Atribut dalam kelas Ticket dan PremiumTicket yang digunakan untuk menyimpan nomor kursi yang dipilih pengguna.

- ticketID: Atribut dalam kelas Ticket dan PremiumTicket yang digunakan untuk menyimpan ID tiket
- paymentStatus: Atribut dalam kelas Ticket dan PremiumTicket yang digunakan untuk menyimpan status pembayaran tiket.

4. Objek:
   - signUp: Objek dari kelas `SignUp` yang digunakan untuk melakukan pendaftaran pengguna baru.
   - login: Objek dari kelas `Login` yang digunakan untuk melakukan otentikasi pengguna.
   - movie: Objek dari kelas `Movie` yang digunakan untuk merepresentasikan informasi tentang sebuah film, seperti nama film.
   - genre: Objek dari kelas `Genre` yang digunakan untuk merepresentasikan genre film.
   - date: Objek dari kelas `Date` yang digunakan untuk merepresentasikan tanggal tayang film.
   - time: Objek dari kelas `Time` yang digunakan untuk merepresentasikan waktu tayang film.
   - cinema: Objek dari kelas `Cinema` yang digunakan untuk merepresentasikan informasi tentang sebuah bioskop, seperti nama bioskop dan harga tiket.
   - cinemaList: Objek dari kelas `CinemaList` yang digunakan untuk menyimpan daftar bioskop.
   - studio: Objek dari kelas `Studio` yang digunakan untuk merepresentasikan informasi tentang sebuah studio dalam sebuah bioskop.
   - seatNumber: Objek dari kelas `SeatNumber` yang digunakan untuk merepresentasikan nomor kursi.
   - ticket: Objek dari kelas `Ticket` yang digunakan untuk merepresentasikan informasi tentang tiket, seperti film yang ditonton, bioskop, jumlah tiket, dan harga tiket.
   - premiumTicket: Objek dari kelas `PremiumTicket` yang merupakan turunan dari kelas `Ticket` dan digunakan untuk merepresentasikan informasi tentang tiket premium dengan tambahan harga snack.
   - paymentMethod: Objek dari kelas `PaymentMethod` yang digunakan untuk merepresentasikan metode pembayaran.

5. Properti:
   - name: Properti dalam kelas `Movie`, `Genre`, `Cinema`, dan `Studio` yang digunakan untuk mengakses dan mengubah nama film, genre film, nama bioskop, dan nama studio.
   - ticketPrice: Properti dalam kelas `Cinema` yang digunakan untuk mengakses dan mengubah harga tiket bioskop.
   - cinemas: Properti dalam kelas `CinemaList` yang digunakan untuk mengakses dan mengubah daftar bioskop.
   - number: Properti dalam kelas `SeatNumber` yang digunakan untuk mengakses dan mengubah nomor kursi.
   - movie, cinema: Properti dalam kelas `Ticket` yang digunakan untuk mengakses dan mengubah objek film dan bioskop.
   - ticketQuantity: Properti dalam kelas `Ticket` yang digunakan untuk mengakses dan mengubah jumlah tiket.
   - totalPrice: Properti dalam kelas `Ticket` dan `PremiumTicket` yang digunakan untuk mengakses harga total tiket.
   - snackPrice: Properti dalam kelas `PremiumTicket` yang digunakan untuk mengakses dan mengubah harga snack.
   - methodName: Properti dalam kelas `PaymentMethod` yang digunakan untuk mengakses dan mengubah nama metode pembayaran.

Konsep OOP melibatkan empat prinsip utama: encapsulation, inheritance, polymorphism, dan abstraction.

1. Enkapsulasi (Encapsulation):
Enkapsulasi mengacu pada konsep menyembunyikan rincian implementasi dari objek dan hanya memperlihatkan fungsi-fungsi publik yang dapat diakses oleh pengguna. Dalam program ini, kelas-kelas seperti SignUp, Login, Movie, Genre, Cinema, Studio, CinemaList, SeatNumber, Ticket, dan PremiumTicket menggambarkan penggunaan enkapsulasi. Properti-properti dan metode-metode yang sensitif seperti email, password, harga tiket, dan metode otentikasi terlindungi dan hanya dapat diakses melalui metode publik yang disediakan oleh kelas tersebut.

2. Pewarisan (Inheritance):
Pewarisan memungkinkan kelas untuk mewarisi properti dan metode dari kelas lain. Dalam program yang dibuat, terdapat hubungan pewarisan antara kelas Genre dan Movie, di mana kelas Genre mewarisi kelas Movie. Hal ini memungkinkan kelas Genre untuk memiliki properti genre dan metode yang terkait dengan jenis genre tertentu, sementara mewarisi metode lain yang ada pada kelas Movie.

3. Polimorfisme (Polymorphism):
Polimorfisme memungkinkan objek untuk mengambil banyak bentuk, artinya objek yang berbeda dapat merespons metode yang sama dengan cara yang berbeda. Contoh polimorfisme dapat dilihat pada penggunaan kelas Ticket dan PremiumTicket. Kedua kelas ini mewarisi dari kelas Ticket, namun masing-masing memiliki implementasi metode getTotalPrice() yang berbeda. Hal ini memungkinkan objek dari kelas Ticket dan PremiumTicket untuk merespons metode getTotalPrice() dengan perilaku yang sesuai dengan jenis tiket yang mereka wakili.

4. Abstraksi (Abstraction):
Abstraksi mengacu pada proses menyederhanakan kompleksitas dengan menyembunyikan detail yang tidak relevan dari pengguna. Dalam program yang dibuat, kelas-kelas seperti SignUp, Login, Movie, Cinema, Studio, dan PaymentMethod menyediakan abstraksi untuk fitur-fitur terkait seperti pendaftaran pengguna, otentikasi, film, bioskop, studio, dan metode pembayaran. Pengguna program hanya perlu berinteraksi dengan metode-metode publik yang disediakan oleh kelas-kelas ini tanpa perlu mengetahui rincian implementasinya.

Melalui penerapan keempat pilar ini, program dapat dibangun dengan struktur yang terorganisir, termodular, dan lebih mudah dipahami. Pilar-pilar ini memungkinkan pengembang untuk merancang program dengan cara yang lebih fleksibel, efisien, dan dapat diperluas di masa mendatang.

Keuntungan OOP termasuk modularitas (memudahkan pengelolaan dan perbaikan program), reusabilitas (menghemat waktu dan upaya pengembangan), skaalabilitas (dapat diperluas dan dimodifikasi dengan mudah), pemahaman yang lebih baik (fokus pada konsep yang relevan), dan pemecahan masalah yang intuitif (sesuai dengan cara berpikir manusia dan dunia nyata).


# No.4

Encapsulation:
Enkapsulasi mengacu pada konsep menyembunyikan rincian implementasi dari objek dan hanya memperlihatkan fungsi-fungsi publik yang dapat diakses oleh pengguna. Dalam program ini, kelas-kelas seperti SignUp, Login, Movie, Genre, Cinema, Studio, CinemaList, SeatNumber, Ticket, dan PremiumTicket menggambarkan penggunaan enkapsulasi. Properti-properti dan metode-metode yang sensitif seperti email, password, harga tiket, dan metode otentikasi terlindungi dan hanya dapat diakses melalui metode publik yang disediakan oleh kelas tersebut.

Dalam program yang saya buat, enkapsulasi diimplementasikan untuk menyembunyikan rincian implementasi yang sensitif atau tidak relevan dari objek-objek yang terlibat. Enkapsulasi memungkinkan pengguna program hanya berinteraksi dengan antarmuka publik yang ditentukan oleh kelas-kelas tersebut.

Berikut adalah contoh implementasi enkapsulasi dalam program tersebut:

1. Kelas SignUp dan Login:
Kelas SignUp dan Login digunakan untuk mendaftarkan pengguna baru dan mengotentikasi pengguna yang sudah terdaftar. Properti email dan password yang sensitif disimpan sebagai variabel pribadi (private) dalam kelas ini. Pengguna program tidak dapat mengakses langsung nilai dari email dan password tersebut. Sebagai gantinya, kelas menyediakan metode publik (public) seperti setEmail(), setPassword(), dan authenticate() yang dapat digunakan oleh pengguna program untuk mengatur dan memeriksa email serta password.

2. Kelas Movie:
Kelas Movie merepresentasikan sebuah film. Properti seperti title (judul), director (sutradara), dan duration (durasi) disimpan sebagai variabel pribadi dalam kelas ini. Untuk mengakses nilai-nilai properti ini, kelas Movie menyediakan metode-metode publik seperti getTitle(), getDirector(), dan getDuration(). Hal ini memastikan bahwa nilai-nilai properti hanya dapat diakses melalui metode-metode publik yang ditentukan oleh kelas Movie.

3. Kelas Genre:
Kelas Genre mewarisi dari kelas Movie dan memiliki properti tambahan yaitu genre (jenis genre film). Properti ini juga diimplementasikan sebagai variabel pribadi. Untuk mengakses nilai genre, kelas Genre menyediakan metode publik seperti getGenre(). Pengguna program hanya dapat mengakses nilai genre melalui metode publik ini.

4. Kelas Cinema dan Studio:
Kelas Cinema merepresentasikan sebuah bioskop, sedangkan kelas Studio merepresentasikan sebuah studio di bioskop tersebut. Kedua kelas ini memiliki properti-properti seperti cinemaName (nama bioskop) dan studioNumber (nomor studio). Properti-properti ini disimpan sebagai variabel pribadi dalam masing-masing kelas dan dapat diakses melalui metode-metode publik yang sesuai seperti getCinemaName(), getStudioNumber(), dan setStudioNumber(). Dengan menggunakan metode-metode ini, pengguna program hanya berinteraksi dengan nilai-nilai properti melalui antarmuka publik yang telah ditentukan.

Dalam semua kasus di atas, enkapsulasi memastikan bahwa rincian implementasi yang sensitif atau tidak relevan seperti email, password, dan rincian properti lainnya tetap terlindungi dan hanya dapat diakses melalui metode-metode publik yang disediakan oleh kelas-kelas tersebut. Hal ini membantu menjaga keamanan program dan mencegah kesalahan yang disebabkan oleh manipulasi langsung terhadap variabel-variabel pribadi.

Berikut source code yang diimplementasikan dalam encapsulation terdapat pada class SignUp, class Login, class Movie, class Cinema, class Studio.

[MTix](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/MTix.java)


# No.5
Abstraction:
Abstraksi mengacu pada proses menyederhanakan kompleksitas dengan menyembunyikan detail yang tidak relevan dari pengguna. Dalam program ini, kelas-kelas seperti SignUp, Login, Movie, Cinema, Studio, dan PaymentMethod menyediakan abstraksi untuk fitur-fitur terkait seperti pendaftaran pengguna, otentikasi, film, bioskop, studio, dan metode pembayaran. Pengguna program hanya perlu berinteraksi dengan metode-metode publik yang disediakan oleh kelas-kelas ini tanpa perlu mengetahui rincian implementasinya.

Dalam program yang saya buat, abstraksi digunakan untuk menyembunyikan kompleksitas rincian implementasi yang tidak relevan dan fokus pada fitur dan fungsi yang penting dari objek-objek yang terlibat. Abstraksi membantu pengguna program untuk lebih mudah memahami dan menggunakan objek-objek tersebut dengan menyediakan tingkat pemahaman yang lebih tinggi melalui antarmuka yang lebih sederhana.

Berikut adalah contoh implementasi abstraksi dalam program tersebut:

1. Kelas SignUp dan Login:
Kelas SignUp dan Login merupakan abstraksi dari proses pendaftaran pengguna baru dan autentikasi pengguna yang sudah terdaftar. Pengguna program tidak perlu mengetahui atau memahami rincian implementasi internal dari kedua proses ini. Sebagai gantinya, kelas-kelas ini menyediakan metode-metode publik seperti signUp() dan login() yang mengabstraksikan kompleksitas dan rincian internal yang terkait dengan proses pendaftaran dan autentikasi.

2. Kelas Movie:
Kelas Movie merupakan abstraksi dari sebuah film. Properti-properti seperti judul, sutradara, dan durasi dianggap sebagai fitur-fitur utama yang relevan dengan sebuah film. Melalui metode-metode publik seperti getTitle(), getDirector(), dan getDuration(), pengguna program dapat mengakses informasi-informasi tersebut tanpa perlu tahu atau memahami bagaimana properti-properti tersebut diimplementasikan atau dikelola secara internal.

3. Kelas Genre:
Kelas Genre merupakan abstraksi dari genre film. Properti genre merupakan informasi yang relevan dan terkait dengan sebuah film. Dengan menggunakan metode publik getGenre(), pengguna program dapat memperoleh informasi genre tanpa perlu mengetahui atau memahami implementasi internal dari kelas Genre.

4. Kelas Cinema dan Studio:
Kelas Cinema dan Studio adalah abstraksi dari sebuah bioskop dan studio di dalamnya. Properti-properti seperti nama bioskop dan nomor studio dianggap sebagai atribut-atribut utama yang terkait dengan sebuah bioskop dan studio. Melalui metode-metode publik seperti getCinemaName(), getStudioNumber(), dan setStudioNumber(), pengguna program dapat berinteraksi dengan informasi-informasi ini tanpa harus terlibat dalam kompleksitas dan rincian internal terkait manajemen bioskop dan studio.

Dalam semua kasus di atas, abstraksi membantu menyederhanakan kompleksitas program dengan menyembunyikan rincian implementasi yang tidak relevan. Hal ini memungkinkan pengguna program untuk berfokus pada fitur dan fungsionalitas yang penting, dan memahami objek-objek tersebut dengan tingkat pemahaman yang lebih tinggi melalui antarmuka yang lebih sederhana.

Berikut source code yang diimplementasikan dalam abstraction terdapat pada class SignUp, class Login, class Movie, class Cinema, class Studio.

[MTix](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/MTix.java)


# N0.6
Dalam program yang saya buat, terdapat konsep pewarisan (inheritance) dan polimorfisme (polymorphism) yang digunakan untuk membangun hubungan antara kelas-kelas yang berbeda. Berikut adalah penjelasan lebih detail tentang kedua konsep tersebut:

1. Pewarisan (Inheritance):
Pewarisan adalah konsep dalam pemrograman berorientasi objek yang memungkinkan suatu kelas untuk mewarisi properti dan metode dari kelas lain. Dalam program di atas, terdapat pewarisan antara kelas-kelas berikut:

- Kelas SignUp dan Login mewarisi kelas User. Hal ini berarti bahwa kelas SignUp dan Login memiliki semua properti dan metode yang didefinisikan dalam kelas User, sehingga tidak perlu mendefinisikan ulang properti dan metode yang sama.
- Kelas Movie mewarisi kelas Genre. Dengan demikian, kelas Movie juga memiliki properti genre yang telah didefinisikan dalam kelas Genre, dan tidak perlu mendefinisikan ulang properti tersebut.

Pewarisan memungkinkan penggunaan kembali kode yang ada, meningkatkan fleksibilitas, dan memungkinkan hierarki kelas yang lebih terstruktur.

2. Polimorfisme (Polymorphism):
Polimorfisme adalah konsep di mana suatu objek dapat memiliki banyak bentuk. Dalam konteks program di atas, polimorfisme dapat dilihat dalam beberapa aspek:

- Penggunaan polimorfisme terlihat pada penggunaan metode signUp() dan login() pada objek yang bervariasi. Meskipun keduanya adalah objek dari kelas yang berbeda (SignUp dan Login), pengguna program dapat menggunakan metode yang sama untuk melakukan proses pendaftaran dan autentikasi.
- Dalam loop for-each yang digunakan untuk mencetak informasi film, variabel "movie" dapat mereferensikan objek dari kelas Movie maupun kelas turunannya. Ini menunjukkan polimorfisme, di mana objek dengan tipe yang berbeda dapat diperlakukan sebagai objek dengan tipe yang sama dan metode yang sesuai dapat dipanggil tanpa perlu mengetahui tipe objek yang sebenarnya.

Polimorfisme memungkinkan pengguna program untuk memperlakukan objek dengan cara umum tanpa perlu memperhatikan tipe spesifik objek tersebut, sehingga meningkatkan fleksibilitas dan modularitas dalam kode.

Berikut source code yang diimplementasikan dalam Inheritance dan Polymorphism

[MTix](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/blob/main/MTix.java)



# N0.7

Cara mendeskripsikan proses bisnis (kumpulan use case) ke dalam skema OOP dalam program MTix :

Berikut adalah penjelasan lebih detail mengenai implementasi proses bisnis ke dalam skema OOP dalam program MTix:

1. Proses Pendaftaran dan Autentikasi Pengguna:
   - Terdapat kelas `SignUp` yang merepresentasikan pendaftaran pengguna. Kelas ini memiliki atribut `email` dan `password` untuk menyimpan informasi pengguna yang sedang mendaftar. Terdapat metode `register()` yang digunakan untuk mendaftarkan pengguna baru dengan menggunakan informasi yang diberikan.
   - Terdapat kelas `Login` yang merepresentasikan autentikasi pengguna. Kelas ini memiliki atribut `email` dan `password` untuk menyimpan informasi pengguna yang ingin login. Terdapat metode `authenticate()` yang digunakan untuk mengautentikasi pengguna dengan memeriksa kecocokan informasi yang diberikan dengan data pengguna yang ada.

2. Proses Pembelian Tiket:
   - Terdapat kelas `Movie` yang merepresentasikan informasi film. Kelas ini memiliki atribut `movieName` untuk menyimpan nama film yang dipilih oleh pengguna.
   - Terdapat kelas `Cinema` yang merepresentasikan informasi bioskop. Kelas ini memiliki atribut `cinemaName` untuk menyimpan nama bioskop yang dipilih oleh pengguna, serta atribut `ticketPrice` untuk menyimpan harga tiket di bioskop tersebut.
   - Terdapat kelas `Studio` yang merepresentasikan informasi studio. Kelas ini memiliki atribut `studioName` untuk menyimpan nama studio yang dipilih oleh pengguna.
   - Terdapat kelas `SeatNumber` yang merepresentasikan nomor kursi. Kelas ini memiliki atribut `seatNumber` untuk menyimpan nomor kursi yang dipilih oleh pengguna.
   - Terdapat kelas `Ticket` yang merepresentasikan tiket. Kelas ini memiliki atribut `movie` untuk menyimpan objek `Movie` yang dipilih oleh pengguna, atribut `cinema` untuk menyimpan objek `Cinema` yang dipilih oleh pengguna, serta atribut `quantity` untuk menyimpan jumlah tiket yang dibeli oleh pengguna. Kelas ini juga memiliki metode `calculateTotalPrice()` yang digunakan untuk menghitung total harga tiket berdasarkan harga tiket dan jumlah tiket yang dibeli.
   - Terdapat kelas `PremiumTicket` yang merupakan turunan dari kelas `Ticket`. Kelas ini menambahkan atribut `snackPrice` untuk menyimpan harga snack tambahan yang terkait dengan tiket premium. Kelas ini juga memiliki metode `calculateTotalPrice()` yang dioverride untuk menghitung total harga tiket premium dengan memperhitungkan harga snack tambahan.
   - Objek dari kelas-kelas di atas digunakan dalam proses pembelian tiket pada program MTix. Misalnya, ketika pengguna memilih film, objek dari kelas `Movie` dibuat untuk menyimpan informasi film yang dipilih. Hal yang sama berlaku untuk objek dari kelas `Cinema`, `Studio`, `SeatNumber`, dan `Ticket`.

3. Proses Pembayaran:
   - Terdapat kelas `PaymentMethod` yang merepresentasikan metode pembayaran dalam proses pembayaran. Kelas ini memiliki atribut `methodName` yang digunakan untuk menyimpan nama metode pembayaran yang dipilih oleh pengguna.
   - Pada proses pembayaran, objek dari kelas `PaymentMethod` digunakan untuk memilih metode pembayaran yang akan digunakan. Misalnya, jika pengguna memilih metode pembayaran dengan OVO, objek dari kelas `PaymentMethod` akan dibuat dengan atribut `methodName` diatur menjadi "OVO". Objek ini kemudian digunakan dalam proses pembayaran untuk menentukan metode pembayaran yang dipilih oleh pengguna.
   - Selain atribut `methodName`, kelas `PaymentMethod` juga dapat memiliki atribut lainnya yang relevan dengan metode pembayaran tertentu, seperti nomor kartu kredit atau alamat pembayaran. Metode pembayaran juga dapat memiliki metode lainnya, seperti `processPayment()` yang digunakan untuk melakukan pemrosesan pembayaran sesuai dengan metode yang dipilih.
   - Dengan menggunakan objek dari kelas `PaymentMethod`, program MTix dapat mengelola proses pembayaran dengan mengakses informasi metode pembayaran yang dipilih oleh pengguna dan menjalankan langkah-langkah pembayaran yang sesuai.
   - Pengguna dapat memilih metode pembayaran melalui antarmuka pengguna pada program dan program akan menggunakan objek dari kelas `PaymentMethod` untuk merepresentasikan pilihan tersebut dan melanjutkan dengan proses pembayaran sesuai dengan metode yang dipilih.
   
5. Keluar:
   - Pada menu "Keluar", pengguna dapat keluar dari program dengan mengatur variabel `exit` menjadi `true`.
   - Keuntungan: Mengelola opsi keluar dari program secara terpisah sehingga menghindari kode berulang dan memperjelas logika program.

6. Kelas MTix (kelas utama):
- Kelas `MTix` berfungsi sebagai kelas utama yang menjalankan program dan mengatur interaksi antara pengguna dengan objek-objek lainnya.
- Pada metode `main()`, dilakukan inisialisasi objek `Scanner` untuk input pengguna.
- Setelah pengguna berhasil login, program akan masuk ke dalam loop utama yang berisi menu-menu yang dapat dipilih oleh pengguna.
- Pengguna memilih menu dengan memasukkan pilihan (1, 2, 3, 4, 5 atau 6).
- Berdasarkan pilihan yang dimasukkan, program akan memanggil metode-metode yang sesuai pada objek-objek yang relevan, seperti menampilkan daftar film, daftar bioskop, daftar studio, melakukan pembelian tiket, atau melakukan pembayaran.
- Keuntungan: Kelas `MTix` sebagai kelas utama bertanggung jawab mengatur alur program dan menghubungkan antara objek-objek yang terlibat dalam proses bisnis.

Dengan menggunakan skema OOP seperti di atas, program MTix menjadi lebih terstruktur, modular, dan mudah dimengerti. Setiap entitas dalam proses bisnis direpresentasikan oleh kelas-kelas terpisah yang memiliki tanggung jawab spesifik. Keuntungan dari pendekatan ini antara lain:

1. Modularitas: Proses bisnis yang kompleks dibagi menjadi kelas-kelas yang terpisah, sehingga setiap kelas bertanggung jawab hanya pada tugas tertentu. Hal ini memudahkan pemeliharaan dan pengembangan program di masa depan.

2. Reusabilitas: Dengan menerapkan konsep inheritance, beberapa kelas seperti `PremiumTicket` dapat mewarisi sifat dan fungsi dari kelas induk `Ticket`. Hal ini mengurangi duplikasi kode dan memungkinkan penggunaan ulang yang efisien.

3. Polymorphism: Melalui konsep polymorphism, objek-objek yang memiliki hierarki kelas yang sama, seperti `Ticket` dan `PremiumTicket`, dapat diperlakukan secara seragam dan digunakan secara interchangeable. Misalnya, objek `PremiumTicket` dapat digunakan di tempat objek `Ticket` jika dibutuhkan.

4. Abstraksi: Setiap kelas merepresentasikan entitas nyata dalam proses bisnis. Informasi dan perilaku yang terkait dengan entitas tersebut diabstraksikan ke dalam kelas, sehingga memudahkan pemahaman dan manipulasi data terkait.


# N0.8

Class Diagram:

[Class Diagram](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/Untitled_Diagram-Page-4.drawio__5_.png)

Berikut adalah penjelasan rinci mengenai setiap kelas dalam diagram di atas:

1. MTix:
   - Kelas utama yang berisi metode `main` untuk menjalankan program.
   - Mengatur alur program dengan menu dan logika penggunaan aplikasi MTix.

2. SignUp:
   - Kelas yang merepresentasikan proses pendaftaran pengguna.
   - Memiliki atribut `email` dan `password` untuk menyimpan informasi pengguna.
   - Metode `register()` digunakan untuk melakukan pendaftaran.

3. Login:
   - Kelas yang merepresentasikan proses login pengguna.
   - Memiliki atribut `email` dan `password` untuk menyimpan informasi pengguna.
   - Metode `authenticate()` digunakan untuk melakukan otentikasi.

4. Movie:
   - Kelas yang merepresentasikan informasi tentang sebuah film.
   - Memiliki atribut `name` untuk menyimpan nama film.

5. Genre:
   - Subkelas dari kelas `Movie` yang menambahkan atribut `genre` untuk menyimpan informasi tentang genre film.

6. Date:
   - Kelas yang merepresentasikan tanggal, terdiri dari atribut `day`, `month`, dan `year`.

7. Time:
   - Kelas yang merepresentasikan waktu, terdiri dari atribut `hours` dan `minutes`.

8. Cinema:
   - Kelas yang merepresentasikan informasi tentang sebuah bioskop.
   - Memiliki atribut `name` untuk menyimpan nama bioskop dan `ticketPrice` untuk menyimpan harga tiket.

9. CinemaList:
   - Kelas yang menyimpan daftar nama-nama bioskop.
   - Memiliki atribut `cinemas` yang merupakan array string dari daftar nama-nama bioskop.

10. Studio:
    - Kelas yang merepresentasikan sebuah studio dalam sebuah bioskop.
    - Memiliki atribut `name` untuk menyimpan nama studio.

11. SeatNumber:
    - Kelas yang merepresentasikan nomor kursi.
    - Memiliki atribut `number` untuk menyimpan nomor kursi.

12. Ticket:
    - Kelas yang merepresentasikan tiket film.
    - Memiliki atribut `movie` untuk menyimpan informasi tentang film, `cinema` untuk menyimpan informasi tentang bioskop, dan `ticketQuantity` untuk menyimpan jumlah tiket yang dibeli.
    - Metode `getTotalPrice()` menghitung total harga tiket.

13. PremiumTicket:
    - Subkelas dari kelas `Ticket` yang menambahkan atribut `snackPrice` untuk menyimpan harga snack.
    - Metode `getTotalPrice()` menghitung total harga tiket premium dengan menambahkan harga snack.

14. PaymentMethod:
    - Kelas yang merepresentasikan metode pembayaran.
    - Memiliki atribut `methodName` untuk menyimpan nama metode pembayaran.

Penjelasan lainnya :
- `Login`, `SignUp`, dan `PaymentMethod` merupakan kelas yang berfungsi untuk proses otentikasi pengguna dan metode pembayaran.
- Kelas `Date` dan `Time` digunakan untuk merepresentasikan informasi tanggal dan waktu dalam program, meskipun tidak digunakan secara eksplisit dalam program utama.
- Kelas CinemaList digunakan untuk menyimpan daftar nama-nama bioskop yang tersedia.
- Kelas Genre merupakan subkelas dari kelas Movie yang menambahkan atribut genre untuk menyimpan informasi tentang genre film.
- Kelas Studio digunakan untuk merepresentasikan sebuah studio dalam sebuah bioskop.
- Kelas SeatNumber merepresentasikan nomor kursi dalam bioskop.
- Kelas Ticket digunakan untuk merepresentasikan tiket film dengan atribut yang mencakup informasi tentang film, bioskop, dan jumlah tiket yang dibeli. Juga memiliki metode getTotalPrice() untuk menghitung total harga tiket.
- Kelas PremiumTicket merupakan subkelas dari Ticket yang menambahkan atribut snackPrice untuk menyimpan harga snack. Juga memiliki metode getTotalPrice() yang menghitung total harga tiket premium dengan menambahkan harga snack.



Use Case Table :
[Use Case](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/Use_Case_Table__1_.pdf)

# No.9
[Link](https://youtu.be/u80nCfXRxAo)
# No.10
[Link 1](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc1.PNG)
[Link 2](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc2.PNG)
[Link 3](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc3.PNG)
[Link 4](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc4.PNG)
[Link 5](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc5.PNG)
[Link 6](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc6.PNG)
[Link 7](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc7.PNG)
[Link 8](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc8.PNG)
[Link 9](https://gitlab.com/dealestari0201/pemprograman-berorientasi-objek/-/raw/main/sc9.PNG)
